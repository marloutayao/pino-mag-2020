<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta
			name="viewport"
			content="width=device-width, initial-scale=1.0,
	maximum-scale=1.0"
		/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

		<title>PINO MAG DRAFT</title>

		<!-- fontawesome icons -->
		<link
			rel="stylesheet"
			href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
			integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
			crossorigin="anonymous"
		/>

		<!-- Latest compiled and minified CSS -->
		<link
			rel="stylesheet"
			href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		/>

		<!-- external css -->
		<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri()?>/assets/css/style.css" />

	
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
		<!-- jQuery library -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
	</head>
	
	<body>
	<?php 
		global $post;
		$post_slug = $post->post_name;	
	?>
		<nav>
			<div class="upper-nav">
				<!-- UPPER NAV -->
				<div class="search">
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="20"
						height="20"
						viewBox="0 0 20 20"
						fill="none"
					>
						<circle
							cx="7.875"
							cy="7.875"
							r="7.375"
							stroke="#010202"
						/>
						<path d="M13 13L19 19" stroke="#010202" />
					</svg>
					<label for="search">SEARCH</label>
				</div>
				<div class="pinomag-logo">
					<img
						src="<?= get_template_directory_uri()?>/assets/img/pinomag_logo_black.png"
						alt="PINO Magazine LOGO"
					/>
				</div>

				<div class="nav-icons">
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="20"
						height="20"
						viewBox="0 0 20 20"
						fill="none"
					>
						<circle
							cx="7.875"
							cy="7.875"
							r="7.375"
							stroke="#010202"
						/>
						<path d="M13 13L19 19" stroke="#010202" />
					</svg>
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="20"
						height="20"
						viewBox="0 0 20 20"
						fill="none"
					>
						<circle
							cx="7.875"
							cy="7.875"
							r="7.375"
							stroke="#010202"
						/>
						<path d="M13 13L19 19" stroke="#010202" />
					</svg>
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="20"
						height="20"
						viewBox="0 0 20 20"
						fill="none"
					>
						<circle
							cx="7.875"
							cy="7.875"
							r="7.375"
							stroke="#010202"
						/>
						<path d="M13 13L19 19" stroke="#010202" />
					</svg>
				</div>
				<!-- END OF UPPER NAV -->
			</div>

			<div class="middle-nav">
				<ul>
					
					<li><a href="<?= home_url(); ?>" class="<?= ($post_slug == "home" ? "active-middle-nav" : "none" ) ?>" >JOURNAL</a></li>
					<li><a href="<?= get_template_directory_uri()?>/shop" class="<?= ($post_slug == "shop" ? "active-middle-nav" : "none" ) ?>" >SHOP</a></li>
					<li><a href="<?= get_template_directory_uri()?>/our-story" class="<?= ($post_slug == "our-story" ? "active-middle-nav" : "none" ) ?>" >OUR STORY</a></li>
				</ul>
			</div>

			<div class="bottom-nav">
				<ul>
					<li><a href="#">LATEST</a></li>
					<li><a href="<?= get_template_directory_uri()?>/architecture">ARCHITECTURE</a></li>
					<li><a href="#">ART</a></li>
					<li><a href="#">DESIGN</a></li>
					<li><a href="#">FASHION</a></li>
					<li><a href="#">FOOD</a></li>
					<li><a href="#">LIFSTYLE</a></li>
					<li><a href="#">MEDIA</a></li>
				</ul>
			</div>
		</nav>
		<!-- END OF NAVBAR -->