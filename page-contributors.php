<?php get_header() ?>
<?php 
		global $post;
		$post_slug = $post->post_name;	
?>
<div class="content-container">
	<div class="small-hero-header">
		<div class="overlay-text">
			<h1>Welcome to PINO</h1>
		</div>
	</div>
</div>

<div class="content-container our-story-nav">
	<ul>
		<li><a href="<?= get_template_directory_uri()?>/our-story">Our Story</a></li>
		<li><a class="<?= ($post_slug == "contributors" ? "active-underline-nav" : "none" ) ?>" href="<?= get_template_directory_uri()?>/contributors">Contributors</a></li>
		<li><a href="<?= get_template_directory_uri()?>/advertising">Advertising</a></li>
		<li><a href="<?= get_template_directory_uri()?>/careers">Careers</a></li>
	</ul>
</div>

<div class="content-container">
    <div class="our-story-container">
       <div class="our-story-description">
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. In porro fugiat ducimus illum atque veritatis expedita temporibus magni! Dolor impedit provident tempore! Est, quasi! Nihil minus aperiam unde minima laborum.
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Error cum qui natus eveniet! Doloremque ducimus facilis eveniet molestias, impedit quidem sunt amet iusto vitae illum distinctio, inventore quos consequuntur quibusdam!
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nobis animi ea provident molestias corporis ducimus mollitia eveniet omnis veniam inventore ipsam, eum ipsa labore impedit quasi atque id iusto aut.
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ex natus dolore ratione nesciunt, recusandae sequi officia. Expedita eveniet, dignissimos ducimus consectetur omnis, mollitia eligendi consequatur rerum, quis iusto illo maiores.
            </p>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. In porro fugiat ducimus illum atque veritatis expedita temporibus magni! Dolor impedit provident tempore! Est, quasi! Nihil minus aperiam unde minima laborum.
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Error cum qui natus eveniet! Doloremque ducimus facilis eveniet molestias, impedit quidem sunt amet iusto vitae illum distinctio, inventore quos consequuntur quibusdam!
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nobis animi ea provident molestias corporis ducimus mollitia eveniet omnis veniam inventore ipsam, eum ipsa labore impedit quasi atque id iusto aut.
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ex natus dolore ratione nesciunt, recusandae sequi officia. Expedita eveniet, dignissimos ducimus consectetur omnis, mollitia eligendi consequatur rerum, quis iusto illo maiores.
            </p>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. In porro fugiat ducimus illum atque veritatis expedita temporibus magni! Dolor impedit provident tempore! Est, quasi! Nihil minus aperiam unde minima laborum.
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Error cum qui natus eveniet! Doloremque ducimus facilis eveniet molestias, impedit quidem sunt amet iusto vitae illum distinctio, inventore quos consequuntur quibusdam!
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nobis animi ea provident molestias corporis ducimus mollitia eveniet omnis veniam inventore ipsam, eum ipsa labore impedit quasi atque id iusto aut.
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ex natus dolore ratione nesciunt, recusandae sequi officia. Expedita eveniet, dignissimos ducimus consectetur omnis, mollitia eligendi consequatur rerum, quis iusto illo maiores.
            </p>
        </div>
        
        <div class="our-story-partners">
        <p class="bold-title">WANT TO APPLY?</p>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis porro eum, voluptate quam quae beatae quaerat dolore consequuntur veniam possimus deleniti, hic ea ex, blanditiis debitis quasi similique labore autem!
            </p>
           <a style="display:block" href="#" class="anchor-button">Apply Now</a>
           <a style="display:block" href="#" class="anchor-button">Contact Us</a>
            
            
        </div>
    </div>
</div>
<?php get_footer() ?>