<?php get_header() ?>

<div class="content-container inner-journal">
    <h4>Architecture</h4>
    <div class="journal-info">
        <h6>3 MINUTE READ</h6>
        <h2>TITLE: Subtitle</h2>
    </div>
    <div class="journal-subinfo">
    <h6>by Writer</h6>
        <h6>Date</h6>
    </div>
    
    <img src="<?= get_template_directory_uri()?>/assets/img/5.jpg" alt="">
</div>

<div class="content-container journal-description">
    <div class="journal-text">
        <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis eligendi doloremque sit modi voluptatum perferendis consequatur ducimus ex distinctio obcaecati doloribus voluptatem non quisquam vel magni placeat, totam excepturi nesciunt?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, odio! Blanditiis illum pariatur laudantium quibusdam mollitia veniam aliquam obcaecati quaerat. Voluptatum laboriosam officia aperiam dolor est molestias quo dignissimos? Repellat.
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Id similique repudiandae nobis blanditiis quos perferendis, vitae maiores saepe accusantium modi impedit quod ratione aliquid eum unde. Voluptatibus perspiciatis eum voluptate.
        </p>
        <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis eligendi doloremque sit modi voluptatum perferendis consequatur ducimus ex distinctio obcaecati doloribus voluptatem non quisquam vel magni placeat, totam excepturi nesciunt?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, odio! Blanditiis illum pariatur laudantium quibusdam mollitia veniam aliquam obcaecati quaerat. Voluptatum laboriosam officia aperiam dolor est molestias quo dignissimos? Repellat.
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Id similique repudiandae nobis blanditiis quos perferendis, vitae maiores saepe accusantium modi impedit quod ratione aliquid eum unde. Voluptatibus perspiciatis eum voluptate.
        </p>
        <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis eligendi doloremque sit modi voluptatum perferendis consequatur ducimus ex distinctio obcaecati doloribus voluptatem non quisquam vel magni placeat, totam excepturi nesciunt?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, odio! Blanditiis illum pariatur laudantium quibusdam mollitia veniam aliquam obcaecati quaerat. Voluptatum laboriosam officia aperiam dolor est molestias quo dignissimos? Repellat.
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Id similique repudiandae nobis blanditiis quos perferendis, vitae maiores saepe accusantium modi impedit quod ratione aliquid eum unde. Voluptatibus perspiciatis eum voluptate.
        </p>
        <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis eligendi doloremque sit modi voluptatum perferendis consequatur ducimus ex distinctio obcaecati doloribus voluptatem non quisquam vel magni placeat, totam excepturi nesciunt?
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, odio! Blanditiis illum pariatur laudantium quibusdam mollitia veniam aliquam obcaecati quaerat. Voluptatum laboriosam officia aperiam dolor est molestias quo dignissimos? Repellat.
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Id similique repudiandae nobis blanditiis quos perferendis, vitae maiores saepe accusantium modi impedit quod ratione aliquid eum unde. Voluptatibus perspiciatis eum voluptate.
        </p>
    </div>
</div>

<div class="content-container highlights">
	<h1>DISCOVER MORE</h1>
	<div class="highlights-slider">

		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/1.JPG" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/2.JPG" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/3.JPG" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/4.jpg" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
	</div>
</div>
<?php get_footer() ?>