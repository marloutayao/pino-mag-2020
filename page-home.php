<?php get_header() ?>

<div class="content-container">
	<?php echo do_shortcode('[slide-anything id="64"]'); ?>
</div>

<div class="content-container highlights">
	<h1>HIGHLIGHTS</h1>
	<div class="highlights-slider">

		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/1.JPG" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/2.JPG" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/3.JPG" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
		<a href="#">
			<div>
				<img src="<?= get_template_directory_uri()?>/assets/img/4.jpg" alt="">
				<label>
					<h4>Category</h4>
					<h5>Title: Subtitle</h5>
					<small>3 MINUTE READ</small>
				</label>
			</div>
		</a>
	</div>
</div>

<div class="content-container discover-more">
	<h1>DISCOVER MORE</h1>
	<div class="masonry">
		<a href="#">
			<div class="item">
				<img class="masonry-img" src="<?= get_template_directory_uri()?>/assets/img/1.JPG" >
				<label>
					<h3>Category</h3>
					<h5>Title: Subtitle</h5>
					<h6>3 MINUTE READ</h6>
				</label>
			</div>
		</a>
		<a href="#">
			<div class="item">
				<img class="masonry-img" src="<?= get_template_directory_uri()?>/assets/img/2.JPG" >
				<label>
				<h3>Category</h3>
					<h5>Title: Subtitle</h5>
					<h6>3 MINUTE READ</h6>
				</label>
			</div>		
		</a>
		<a href="#">
			<div class="item">
				<img class="masonry-img" src="<?= get_template_directory_uri()?>/assets/img/3.JPG" >
				<label>
				<h3>Category</h3>
					<h5>Title: Subtitle</h5>
					<h6>3 MINUTE READ</h6>
				</label>
			</div>
		</a>
		<a href="#">
			<div class="item">
				<img class="masonry-img" src="<?= get_template_directory_uri()?>/assets/img/4.jpg" >
				<label>
				<h3>Category</h3>
					<h5>Title: Subtitle</h5>
					<h6>3 MINUTE READ</h6>
				</label>
			</div>
		</a>
		<a href="#">
			<div class="item">
				<img class="masonry-img" src="<?= get_template_directory_uri()?>/assets/img/5.jpg" >
				<label>
				<h3>Category</h3>
					<h5>Title: Subtitle</h5>
					<h6>3 MINUTE READ</h6>
				</label>
			</div>
		</a>
		<a href="#">
			<div class="item">
				<img class="masonry-img" src="<?= get_template_directory_uri()?>/assets/img/6.jpg" >
				<label>
				<h3>Category</h3>
					<h5>Title: Subtitle</h5>
					<h6>3 MINUTE READ</h6>
				</label>
			</div>
		</a>
	</div>
</div>
<?php get_footer() ?>