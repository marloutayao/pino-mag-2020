<?php

// ADDING JS and CSS files

function gate_setup() {
    wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('style', get_stylesheet_directory_uri().'/assets/css/style.css', NULL, microtime(), "all");
    wp_enqueue_script('app', get_theme_file_uri('/assets/js/app.js'), NULL, microtime(), true);
}

add_action('wp_enqueue_scripts', 'gate_setup');

// Adding Theme Support

function gate_init() {
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support('html5', 
        array('comment-list', 'comment-form', 'search-form')
    );
}

add_action('after_setup_theme', 'gate_init');


// Product Cards Post Type

function gate_custom_post_type() {
    register_post_type('product',
        array(
            'rewrite' => array('slug' => 'products_card'),
            'labels' => array(
                'name' => 'Products Card',
                'singular_name' => 'Product',
                'add_new_item' => 'Add New Card',
                'edit_item' => 'Edit Card'
            ),
            'menu-icon' => 'dashicons-clipboard',
            'public' => true,
            'has_archive' => true,
            'supports' => array(
                'title', 'thumbnail', 'editor', 'excerpt'
            )
        )
    );
}

add_action('init', 'gate_custom_post_type');